#!/bin/bash
proc="$1"
var=`ps -A | grep $proc | awk '{printf ("%s\n",$1)}'`

for result in $var
do
echo "Processing $result"; kill -9 $result; 
   
done
ps -ef | grep defunct | grep -v grep | cut -b9-20 | xargs kill -9
