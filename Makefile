SHELL := /bin/bash

CC = gcc 

CFLAGS = -Wall -O2 

#all: doc compress clean



all: servidor cloudshell relatorio.pdf compress clean 

clean: 
	@echo "A limpar diretoria...."
	rm -rf *.o
	rm -rf trunk/*.o
	rm -rf report/*.toc 
	rm -rf report/*.aux   
	rm -rf report/*.log  

	#rm -rf ./doc

clean2:
	@echo "A eliminar executáveis...."
	rm -f servidor 
	rm -f cshell
	rm -rf LEI12.zip
	rm -rf relatorio.pdf

start_server:
	./servidor &

start_cloud_shell:
	./cshell

############################# - Documentação - #########################################

relatorio.pdf:  report/rel.tex 
	latexmk -pdf report/rel.tex 
	mv rel.pdf relatorio.pdf 
	rm -fr rel.* 



compress: 
	@echo "Preparar ficheiro Zip para entrega...."
	zip -r LEI12.zip trunk Makefile relatorio.pdf testes

############################### - MAIN - ###########################################

servidor: ./trunk/servidor.o ./trunk/common.o
	@echo "A compilar servidor...."
	$(CC) $(CFLAGS) ./trunk/servidor.o ./trunk/common.o -o servidor 

cloudshell: ./trunk/cshell.o ./trunk/common.o 
	@echo "A compilar cloudshell...."
	$(CC) $(CFLAGS) ./trunk/cshell.o ./trunk/common.o -o cshell

cloudshell.o: ./trunk/common.c ./trunk/common.h ./trunk/libs.h ./trunk/cshell.c
	$(CC) $(CFLAGS) -c ./trunk/cshell.c 

servidor.o: ./trunk/common.c ./trunk/common.h ./trunk/libs.h ./trunk/servidor.c
	$(CC) $(CFLAGS) -c ./trunk/servidor.c

common.o: ./trunk/common.c ./trunk/common.h ./trunk/libs.h
	$(CC) $(CFLAGS) -c ./trunk/common.c



