#include <fcntl.h>
#include <stdio.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>

#include <readline/readline.h>
#include <readline/history.h>
#define MAX_BUF 1024

int main()
{   int fd_com, fd_cshell;
    char *fifoCOM = "/tmp/COM";
    char *fifoCSHELL = "/tmp/CSHELL";
    int st = 0, x;
    char *buff = ( char * ) calloc ( MAX_BUF, sizeof ( char ) *MAX_BUF );
    /* Descritores de ficheiro */
    fd_com = open ( fifoCOM, O_RDONLY );
    fd_cshell = open ( fifoCSHELL, O_WRONLY );
    //dup2 ( 1, fd_com );
    //dup2 ( 0, fd_cshell );

    while ( 1 )
    {   //scanf("%s", buff);
        x = read ( 0, buff, MAX_BUF );
        //sprintf ( buf, "%d", i++ );
        write (  fd_cshell, buff, MAX_BUF );
        memset ( buff,'\0',MAX_BUF );

        x = read ( fd_com, buff, MAX_BUF );
        write ( 1, buff, x );
        memset ( buff,'\0',MAX_BUF );
    }

    close ( fd_com );
    close ( fd_cshell );
    return 0;
}












