
#include "libs.h"
#include "common.h"
#include <time.h>

static char userid [MAX];

static char cmd [MAX];
static int fd_com, fd_cshell;
typedef struct par
{

    char userid[MAX];
    int recursos;


} PAR, * PAR_P;
static int child = 0;
void trata ( int sig )
{   int status;

    switch ( sig )
    {   case SIGUSR2 :
            break;

        case SIGINT :
            close ( fd_com );
            close ( fd_cshell );
	    kill(SIGINT, getpid());
            break;
    }

    // utilizar o sigint para fechar e sair do ciclo
}



void main_cycle ( PAR par, int fd_cshell, int fd_com, char *buff, char *cmd )
{   int x = 0;
    int time = 0;
    child =fork();

    if ( child==0 )
    {   
        signal ( SIGUSR2, trata );/* se o filho receber o sinal SIGUSR2, executa a funcao trata */

        pause(); /* o filho fica ‘a espera que cheguem sinais  */

	cmd = strtok(cmd, "\n ");
        printf ( "%s", cmd );
        execlp ( cmd, cmd,   NULL );
        _exit ( 0 );
    }

    else
    {   printf ( "INICIAR SEQUENCIA COM FILHO, %d\n", child );
        strcpy ( par.userid, userid );
        time = proc_stime ( p_proc_pid ( child ) );
        par.recursos =  gettimediff ( time );
        sprintf ( buff, "%s:%d:", par.userid, par.recursos );
        printf ( "USERID %s REC %d\n", par.userid, par.recursos );
        write (  fd_cshell, buff, MAX );
        memset ( buff,'\0',MAX );
        x = read ( fd_com, buff, MAX );
 

        if ( strcmp ( buff, "KO" ) ==0 )
        {   printf ( "NÃO TEM SALDO %s; CONTACTE O ADMINISTRADOR DO SISTEMA!\n", userid );
            kill ( child, SIGTERM );
        }

        else
        {   kill ( child, SIGUSR2 );
        }

        memset ( buff,'\0',MAX );
        par.recursos = 0;
        wait ( 0L );
    }

    child = 0;
}
int main()
{   char *fifoCOM = "/tmp/COM";
    char *fifoCSHELL = "/tmp/CSHELL";
    int st = 0, x;
    PAR par;
    par.recursos = 0;
    char *buff = ( char * ) calloc ( MAX, sizeof ( char ) *MAX );
    /* Descritores de ficheiro */
    fd_com = open ( fifoCOM, O_RDONLY );
    fd_cshell = open ( fifoCSHELL, O_WRONLY );
    printf ( "INTRODUZA USERID\n" );
    scanf ( "%s", userid );

    while ( 1 )
    {   printf ( "INTRODUZA COMANDO\n" );
        x = read ( 0, cmd, MAX );
        main_cycle ( par, fd_cshell, fd_com, buff, cmd );
        memset ( cmd,'\0',MAX );

        signal ( SIGINT, trata );
    }

    return 0;
}






