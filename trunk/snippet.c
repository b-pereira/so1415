#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>

/* chamadas ao sistema: defs e decls essenciais */
//int execl(const char *path, const char *arg0, ..., NULL);
//int execlp(const char *file, const char *arg-1, ..., NULL);
//int execv(const char *path, char *const argv[]);
//int execvp(const char *file, char *const argv[]);
#define READ 0
#define WRITE 1
#define MAX 1024
char *p_simple_exec2 ( char *cmd )
{   int pipefd[2], x;
    char *buff = ( char * ) malloc ( sizeof ( char ) *MAX );
    pipe ( pipefd );
    if ( !fork() )
    {   close ( pipefd[READ] );
        dup2 ( pipefd[WRITE], 1 );
        close ( pipefd[WRITE] ); 
        execlp ( cmd, cmd, NULL );
        _exit ( 0 );
    }

    else
    {   x = read ( pipefd[READ], buff, MAX );
        write ( pipefd[WRITE], buff, x );
        close ( pipefd[WRITE] );
        close ( pipefd[READ] );
        wait ( 0L );
    }

    return buff;
}

int main ( int argc, char *argv[] )
{   int i = 0;
    int fd;
    //int pipefd1[2];
    int pipefd2[2];
    char *buff;
    char buff2[MAX];
    int x = 0;
    buff = ( char * ) malloc ( sizeof ( char ) *MAX );
   buff = p_simple_exec2("ls");
printf("%s", buff);

    //i++;
    // }
    // cria pipe com nome [COM]
    // lê linha do pipe (caractere a caractere é melhor para fazer o parse da
    // string)
    // Para cada linha que leu no pipe cria um filho
    // Executa o comando lido do pipe no filho
    // redireciona para pipe com o nome do filho o output
    //
    //
    //
    // enquanto isso comunica com o servidor
    return 0;
}














