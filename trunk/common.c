#include "libs.h"
#include "common.h"
char *p_simple_exec ( char *cmd)
{   int pipefd[2], x;
    char *buff = ( char * ) malloc ( sizeof ( char ) *MAX );
    pipe ( pipefd );

    if ( !fork() )
    {   close ( pipefd[READ] );
        dup2 ( pipefd[WRITE], 1 );
        close ( pipefd[WRITE] );
		cmd = strtok(cmd, "\n ");
        execlp ( cmd, cmd, NULL );
        _exit ( 0 );
    }

    else
    {   x = read ( pipefd[READ], buff, MAX-1 );
        write ( pipefd[WRITE], buff, x );
        close ( pipefd[WRITE] );
        close ( pipefd[READ] );
        wait ( 0L );
    }

    return buff;
}

char *p_proc_pid ( int pid )
{   int pipefd[2], x;
    char *buff = ( char * ) malloc ( sizeof ( char ) *MAX );
    char tmp[MAX];
    pipe ( pipefd );

    if ( !fork() )
    {   close ( pipefd[READ] );
        dup2 ( pipefd[WRITE], 1 );
        close ( pipefd[WRITE] );
        sprintf ( tmp, "/proc/%d/stat", pid );
        execlp ( "cat", "cat",  tmp,  NULL );
        _exit ( 0 );
    }

    else
    {   x = read ( pipefd[READ], buff, MAX );
        write ( pipefd[WRITE], buff, x );
        close ( pipefd[WRITE] );
        close ( pipefd[READ] );
        wait ( 0L );
    }

    return buff;
}
char *p_proc_uptime ()
{   int pipefd[2], x;
    char *buff = ( char * ) malloc ( sizeof ( char ) *MAX );
    char tmp[MAX];
    pipe ( pipefd );

    if ( !fork() )
    {   close ( pipefd[READ] );
        dup2 ( pipefd[WRITE], 1 );
        close ( pipefd[WRITE] );
        execlp ( "cat", "cat", "/proc/uptime" ,  NULL );
        _exit ( 0 );
    }

    else
    {   x = read ( pipefd[READ], buff, MAX );
        write ( pipefd[WRITE], buff, x );
        close ( pipefd[WRITE] );
        close ( pipefd[READ] );
        wait ( 0L );
    }

    return buff;
}
int gettimesinceboot()
{   char *procuptime;
    int sec=0, ssec=0;
    int tickspersec = sysconf ( _SC_CLK_TCK );
    procuptime = p_proc_uptime();
    procuptime = strtok ( procuptime, "  " );
    return atoi ( procuptime );
}

int proc_stime ( char *arg )
{   char *str1, *str2, *token, *subtoken;
    int j;

    for ( j = 1, str1 = arg; ; j++, str1 = NULL )
    {   token = strtok ( str1, " " );

        if ( token == NULL||j==22 )
            break;
    }

    return atoi ( token );
}
int gettimediff ( int x )
{   int sinceboot = gettimesinceboot();
    int tickspersec = sysconf ( _SC_CLK_TCK );
    int running = sinceboot - ( x/tickspersec );
    return running;
}











