/* common.c */

char *p_simple_exec(char *cmd);
char *p_proc_pid(int pid);
char *p_proc_uptime(void);
int gettimesinceboot(void);
int proc_stime(char *arg);
int gettimediff(int x);

