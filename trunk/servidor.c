#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#define MAX 1024
#define READ 0
#define WRITE 1
#define OK "OK\n"
#define KO "KO\n"
typedef struct par
{

    int pid;
    int recursos;


} PAR, * PAR_P;

int main()
{   int fd_com, fd_cshell;
    char *fifoCOM = "/tmp/COM";
    char *fifoCSHELL = "/tmp/CSHELL";
    char *token, * str;
    PAR par;
    par.pid = 0;
    par.recursos = 0;
    char *buff = ( char * ) calloc ( MAX, sizeof ( char ) *MAX );
    int st = 0, x, j;
    int saldo = 50;
    /* Criar os "named-pipes*/
    mkfifo ( fifoCOM, 0666 );
    mkfifo ( fifoCSHELL, 0666 );
    /* Descritores de ficheiro */
    fd_com = open ( fifoCOM, O_WRONLY );
    fd_cshell = open ( fifoCSHELL, O_RDONLY );


    /*Ciclo do servidor */
    while ( 1 )
    {   /* Código */
        buff[x]='\0';

        for ( j = 1, str = buff; ; j++, str = NULL )
        {   token = strtok ( str, ":" );

            if ( token == NULL )
                break;

            if ( j == 1 )  par.pid = atoi ( token );

            if ( j == 2 )  par.recursos = atoi ( token );

            printf ( "%d: %s\n", j, token );
        }

        printf ( "SERV  USERID %d REC %d\n", par.pid, par.recursos );
        saldo-=par.recursos;
        printf ( "SALDO %d\n", saldo );

        if ( saldo>0 )
        {   write ( fd_com, OK, 2 );
        }

        else
        {   write ( fd_com, KO, 2 );
        }

        memset ( buff,'\0',MAX );
        par.pid = 0;
        par.recursos = 0;
        printf ( "MENSAGEM ENVIADA\n" );
    }

    close ( fd_com );
    close ( fd_cshell );
    /* remover os  FIFO */
    unlink ( fifoCOM );
    unlink ( fifoCSHELL );
    return 0;
}
















