#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#define MAX 1024
int main()
{   int fd_com, fd_cshell;
    char *fifoCOM = "/tmp/COM";
    char *fifoCSHELL = "/tmp/CSHELL";
    int st = 0, x;
    char *buff = ( char * ) calloc ( MAX, sizeof ( char ) *MAX );
    
    
    /* Criar os "named-pipes*/
    mkfifo ( fifoCOM, 0666 );
    mkfifo ( fifoCSHELL, 0666 );
    /* Descritores de ficheiro */
    fd_com = open ( fifoCOM, O_WRONLY );
    fd_cshell = open (fifoCSHELL, O_RDONLY );

    /*Ciclo do servidor */
    while ( 1 )
    {   while ( x = read (  fd_com, buff, MAX-1 ) >0 )
        {   buff[MAX]='\0';
	    /* Código */
            //sprintf ( buff, "%d", i++ );
	    printf("%s\n",  buff);
            write (  fd_cshell, buff, x );
            memset ( buff,'\0',MAX );
        }
    }

    close ( fd_com );
    close ( fd_cshell );
    /* remover os  FIFO */
    unlink ( fifoCOM );
    unlink ( fifoCSHELL );
    return 0;
}






