#include <signal.h>
#include <stdio.h>

int seg=0;

void hand (int s)
{
	seg++;
}

void show (int s)
{
	printf("%d:%d:%d\n",seg/3600,seg/60,seg % 60);
}

main()
{
	signal(SIGALRM,hand);
	signal(SIGINT,show);
	while(1) { alarm(1); pause(); }
}

