#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>

/* chamadas ao sistema: defs e decls essenciais */
//int execl(const char *path, const char *arg0, ..., NULL);
//int execlp(const char *file, const char *arg-1, ..., NULL);
//int execv(const char *path, char *const argv[]);
//int execvp(const char *file, char *const argv[]);
#define READ 0
#define WRITE 1
#define MAX 1024
int main ( int argc, char *argv[] )
{   int i = 0;
    int fd;
    //int pipefd1[2];
    int pipefd2[2];
    char *buff;
    char buff2[MAX];
    int x = 0;
    buff = ( char * ) malloc ( sizeof ( char ) *MAX );
    fd = open ( "log.txt", O_CREAT | O_WRONLY, 0666 );
    //pipe ( pipefd1 );
    pipe ( pipefd2 );
    dup2 ( 0, pipefd2[READ] );
    dup2 ( fd, pipefd2[WRITE] );

    //// {
    if ( !fork() )
    {   write ( pipefd2[WRITE], buff, MAX-1 );
        buff[MAX] = '\0';
        close ( pipefd2[WRITE] );
        execlp ( buff, buff, NULL );
        //memset ( buff, sizeof ( char ), MAX );
        exit ( 0 );
    }

    else
    {   while ( x = read ( pipefd2[READ], buff, MAX-1 ) > 0 )
        {   write ( pipefd2[WRITE], buff, MAX-1 );
            buff[MAX] = '\0';
        }

        close ( pipefd2[READ] );
        wait ( 0L );
    }

    //i++;
    // }
    // cria pipe com nome [COM]
    // lê linha do pipe (caractere a caractere é melhor para fazer o parse da
    // string)
    // Para cada linha que leu no pipe cria um filho
    // Executa o comando lido do pipe no filho
    // redireciona para pipe com o nome do filho o output
    //
    //
    //
    // enquanto isso comunica com o servidor
    return 0;
}














