#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#define MAX 1024
#define READ 0
#define WRITE 1
int main()
{   int fd;
    char *myfifo = "/tmp/myfifo";
    int st = 0, x;
    char *buff = ( char * ) calloc ( MAX, sizeof ( char ) *MAX );
    /* create the FIFO (named pipe) */
    mkfifo ( myfifo, 0666 );
    /* write "Hi" to the FIFO */
    fd = open ( myfifo, O_WRONLY );

    while ( x = read (  0, buff, MAX-1 ) >0 )
    {   buff[MAX]='\0';
        x = strlen ( buff );
        write (  fd, buff, x );
        memset ( buff,'\0',MAX );
    }

    close ( fd );
    /* remove the FIFO */
    unlink ( myfifo );
    return 0;
}



