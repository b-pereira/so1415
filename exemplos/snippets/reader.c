#include <fcntl.h>
#include <stdio.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>

#include <readline/readline.h>
#include <readline/history.h>
#define MAX_BUF 1024

int main()
{   int fd;
    char *myfifo = "/tmp/myfifo";
    char *buf = ( char * ) calloc ( MAX_BUF, sizeof ( char ) *MAX_BUF );
    int x = 0;
    /* open, read, and display the message from the FIFO */
    fd = open ( myfifo, O_RDONLY );

    while ( x = read ( fd, buf, MAX_BUF ) >0 )
    {   x = strlen ( buf );
        write (  1, buf, x );
        memset ( buf,'\0',MAX_BUF );
        //write ( 1, buf, x );
    }

    close ( fd );
    return 0;
}




