#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>

/* chamadas ao sistema: defs e decls essenciais */
//int execl(const char *path, const char *arg0, ..., NULL);
//int execlp(const char *file, const char *arg-1, ..., NULL);
//int execv(const char *path, char *const argv[]);
//int execvp(const char *file, char *const argv[]);
#define READ 0
#define WRITE 1
#define MAX 1024
int main ( int argc, char *argv[] )
{   int i = 0;
    int fd;
    int pipefd[2];
    pipe ( pipefd );

    if ( fork() == 0 )
    {   close ( pipefd[READ] ); 
        dup2 ( pipefd[WRITE], 1 ); 
        close ( pipefd[WRITE] ); 
        execlp ( "ls", "ls", NULL );
	_exit(0);
    }

    else
    {   char *buffer = ( char * ) calloc ( MAX, sizeof ( char ) *MAX );
        close ( pipefd[WRITE] ); 

        while ( (fd = read ( pipefd[READ], buffer, MAX-1 )) >0 )
        { 
	write ( 1, buffer, fd );
        }
    }

    // cria pipe com nome [COM]
    // lê linha do pipe (caractere a caractere é melhor para fazer o parse da
    // string)
    // Para cada linha que leu no pipe cria um filho
    // Executa o comando lido do pipe no filho
    // redireciona para pipe com o nome do filho o output
    //
    //
    //
    // enquanto isso comunica com o servidor
    return 0;
}

















