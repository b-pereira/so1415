#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/wait.h>

#include <sys/types.h>

#include <signal.h>

void trata ( int sinal )

{   printf ( "Apanhei o sinal %d \n", sinal );
}



int main()
{   pid_t pid;
    int estado,i;
    pid = fork();

    if ( pid < 0 )
    {   printf ( "Erro!!!\n" );
        exit ( -1 );
    }

    else if ( pid == 0 ) /* FILHO */
    {   printf ( "Estou ‘a espera de sinais\n" );
        signal ( SIGINT, trata ); /* se o filho receber o sinal SIGINT, executa a funcao trata */
        signal ( SIGUSR2, trata );/* se o filho receber o sinal SIGUSR2, executa a funcao trata */

        for ( i=0; i<3; i++ )
            pause(); /* o filho fica ‘a espera que cheguem sinais  */
    }

    else /* PAI */
    {   printf ( "Vou enviar sinais\n" );
        sleep ( 3 ); /* "Adormece" durante 3 segundos */
        kill ( pid, SIGUSR2 ); /* envia ao processo filho o sinal SIGUSR2 */
        sleep ( 3 ); /* "Adormece" durante 3 segundos */
        kill ( pid, SIGINT ); /* envia ao processo filho o sinal SIGINT */
        sleep ( 3 ); /* "Adormece" durante 3 segundos */
        kill ( pid, SIGUSR2 ); /* envia ao processo filho o sinal SIGUSR2 */
        pid = wait ( &estado ); /* espera que o filho termine */
    }

    return 0;
}

