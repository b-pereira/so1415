#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h> /* chamadas ao sistema: defs e decls essenciais */
#include <sys/wait.h>
#include <fcntl.h> /* O_RDONLY, O_WRONLY, O_CREAT, O_* */
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
typedef void ( *sighandler_t ) ( int );
#include <errno.h>
#define PERMS 0666
#define MAX 1024
#define READ 0
#define WRITE 1
#define PERMS 0666
#define MAX 1024
#define READ 0
#define WRITE 1
static int child = -1;

typedef void ( *sighandler_t ) ( int );

void handler ( int sig )
{   
   
}
void trata ( int sig )
{   switch ( sig )
    {   case SIGUSR2 :
            break;

        case SIGINT :
            kill ( child, SIGINT );
            break;
    }
}

int main ( int argc, char const *argv[] )
{   int fdcanal1 = -1;
    int fdcanal2 = -1;
    int rd = -1;
    int i, estado;
    char *buff = ( char * ) malloc ( sizeof ( char ) *MAX );
    mkfifo ( "/tmp/READ", PERMS );
    mkfifo ( "/tmp/WRITE", PERMS );
    //    signal ( SIGALRM, handler );
    signal ( SIGCONT, handler );
    fdcanal1 = open ( "/tmp/READ", O_WRONLY,PERMS );
    fdcanal2 = open ( "/tmp/WRITE", O_RDONLY ,PERMS );

    for ( ;; )
    {   child =fork();

        if ( child==0 )
        {   printf ( "Estou ‘a espera de sinais\n" );
            signal ( SIGUSR2, trata );/* se o filho receber o sinal SIGUSR2, executa a funcao trata */
            signal ( SIGINT, trata );/* se o filho receber o sinal SIGUSR2, executa a funcao trata */

            for ( ; ; )
            {   pause(); /* o filho fica ‘a espera que cheguem sinais  */
                printf ( "------CODIGO--------!\n" );
            }

            exit ( 0 );
        }

        else
        {   printf ( "Iniciar Sequencia com filho, %d\n", child );
            sprintf ( buff, "%d", child );
            printf ( "%s\n", buff );
	    getchar();
	    getchar();
            write ( fdcanal1, buff,  strlen ( buff )+1 );
            memset ( buff, sizeof ( char ), MAX );

            //aqui!!!!


            rd = read ( fdcanal2, buff, strlen ( buff ));
	    buff[strlen ( buff )+1]='\0';
            printf ( "%s %d\n", buff , rd );

            if ( strcmp ( buff, "KO" ) ==0 )
            {   kill ( child, SIGINT );
            }

            else
            {   kill ( child, SIGUSR2 );
            }

            wait ( 0L );
        }
    }

    return 0;
}

























