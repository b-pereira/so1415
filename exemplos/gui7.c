#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAXSTR 16
/* chamadas ao sistema: defs e decls essenciais */
//int execl(const char *path, const char *arg0, ..., NULL);
//int execlp(const char *file, const char *arg0, ..., NULL);
//int execv(const char *path, char *const argv[]);
//int execvp(const char *file, char *const argv[]);

void my_system ( char *var )
{   int size = strlen ( var ), i;
    char **tmp;
    char *str, *subtoken = NULL;
    tmp = ( char ** ) malloc ( sizeof ( char * ) );

    for ( i=0; i< size; i++ )
    {   tmp[i] = ( char * ) malloc ( sizeof ( char ) *size );
    }

    for ( str = var, i=0; ; str = NULL, i++ )
    {   subtoken = strtok ( str, " " );

        tmp[i]=subtoken;

        if ( subtoken == NULL )
            break;

        printf ( " --> %s\n", subtoken );
    }

    if ( fork() ==0 )
    {   execvp ( tmp[0], tmp );
        _exit ( 0 );
    }
    
    if ( tmp[0][i]!='&' ){
         
        wait ( 0L );
    }
       
    for ( i=0; i< size; i++ )
    {   free(tmp[i]);
    }
    free(tmp);
}



int main ( int argc, char *argv[] )
{   int i;
    char str[MAXSTR];
    my_system ( argv[1] );
    return 0;
}

