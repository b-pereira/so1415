#define _GNU_SOURCE
#include <unistd.h> /* chamadas ao sistema: defs e decls essenciais */
#include <fcntl.h> /* O_RDONLY, O_WRONLY, O_CREAT, O_* */
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#define PERMS 0666
#define MAX 1024
//int open(const char *path, int oflag [, mode]);
//ssize_t read(int fildes, void *buf, size_t nbyte);
//ssize_t write(int fildes, const void *buf, size_t nbyte);
//int close(int fildes);
//int dup(int fd);
//int dup2(int fd1, int fd2);
int main ( int argc, char *argv[] )
{   int fd1 = 0,fd2=0, fd3=0, i=0;
    char **buff;
    buff = ( char ** ) malloc ( sizeof ( char * ) *MAX );
    /*close ( 0 );
    close ( 1 );
    close ( 2 );*/
    fd1 = open ( "/etc/passwd", O_CREAT| O_RDONLY, PERMS );
    fd2 = open ( "saida1.txt", O_CREAT| O_WRONLY, PERMS );
    fd3 = open ( "erros1.txt", O_CREAT| O_WRONLY, PERMS );
    dup2 ( fd1, 0 );
    dup2 ( fd2, 1 );
    dup2 ( fd3, 2 );

    if ( fork() ==0 )
    {   read ( fd1, buff, MAX );
        write ( fd2, buff, MAX );
        write ( fd3, buff, MAX );
    }

    wait ( 0L );
    fcloseall ();
    return 0;
}









